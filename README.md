# bootstrap_show_password_rails
This is a simple wrapper for this jQuery Bootstrap based plugin: [bootstrap-show-password](https://github.com/UnitooTeam/bootstrap-show-password)


**Attention**
At the moment the wrapper use Bootstrap 4 plugin. For older versions you should
use the library itself

## Usage
To understand how to use the library itself, check this page to avoid duplicates or outdated [documentation](https://github.com/UnitooTeam/bootstrap-show-password).

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'bootstrap-show-password-rails'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install bootstrap-show-password-rails
```

Later add to your `application.js`:
```javascript
//= require bootstrap-show-password-rails
```

## Contributing
* Fork and pull
* Respect each other

## Credits

* [bootstrap-show-password](https://github.com/wenzhixin/bootstrap-show-password)

## Support

<a href="https://liberapay.com/Unitoo/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

---

> [unitoo.it](https://www.unitoo.it) &nbsp;&middot;&nbsp;
> Mastodon [@unitoo](https://mastodon.uno/@unitoo) &nbsp;&middot;&nbsp;
> GitHub [@UnitooTeam](https://github.com/UnitooTeam)
