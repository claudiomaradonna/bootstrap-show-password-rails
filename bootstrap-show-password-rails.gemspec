$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "bootstrap-show-password-rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bootstrap-show-password-rails"
  s.version     = BootstrapShowPasswordRails::Rails::VERSION
  s.authors     = ["Claudio Maradonna"]
	s.email       = ["claudio@unitoo.pw"]
  s.homepage    = "https://github.com/UnitooTeam/bootstrap-show-password-rails"
  s.summary     = "Plugin to show/hide passwords from fields"
	s.description = s.description
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2"

  s.add_development_dependency "sqlite3", '~> 0'
end
