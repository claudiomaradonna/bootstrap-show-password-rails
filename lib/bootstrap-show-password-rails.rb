require "rails"
require "bootstrap-show-password-rails/version"

module BoostrapShowPasswordRails
  module Rails
    if ::Rails.version.to_s < "3.1"
      require "bootstrap-show-password-rails/railtie"
    else
      require "bootstrap-show-password-rails/engine"
    end
  end
end
